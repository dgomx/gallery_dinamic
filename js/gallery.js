$(document).ready(function() {

	$.ajax({
        url: './assets/gallery/thumbnails',
        success: function (data) {

        	$(data).find('td > a').each(function() {
        		var gImgFile = $(this).attr('href');

        		console.log(gImgFile);
        		
        		if ( gValidateImg(gImgFile) ) {
        			
        			var gImg = '<a href="assets/gallery/' + gImgFile + '" data-lightbox="roadtrip">';
					gImg += '<img src="assets/gallery/thumbnails/' + gImgFile + '"></a>';

        			$('.g-container').append(gImg);

        		}
        		
        	});

        }
    });

    function gValidateImg(file) {

    	var extension = file.substr( (file.lastIndexOf('.') +1) );

    	if (extension == 'jpg') {
    		return true;
    	} else {
    		return false;
    	}

    }

});